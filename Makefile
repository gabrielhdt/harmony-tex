LC   = lualatex
LILY = lilypond-book
.POSIX:

.SUFFIXES:

all: systems.pdf

# Many weird directories are generated
clean:
	rm -rf 46 67 8c 9d a0 snippet-*.ly
	rm -rf systems.aux systems.dep systems.log systems.lytex systems.out

clobber: clean
	rm -f systems.pdf

.SUFFIXES: .lytex .tex .org .pdf

.org.lytex:
	emacs $< --batch --load scrartcl.el --funcall org-latex-export-to-latex
	mv ${<:.org=.tex} $@

.lytex.tex:
	${LILY} --format=latex $<

.tex.pdf:
	${LC} $<
	${LC} $<
