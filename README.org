#+TITLE:   Harmony-tex
#+DATE:    February 23, 2020
#+SINCE:   2020-02-23
#+STARTUP: inlineimages nofold

* Table of Contents :TOC_3:noexport:
- [[#description][Description]]
  - [[#maintainers][Maintainers]]
- [[#prerequisites][Prerequisites]]

* Description
This repository contains notes about harmony in music.

+ Some definitions of notations with examples
+ Harmonic systems
+ Some rules on chord substitution

** Maintainers
+ @koizel

* Prerequisites
+ make (POSIX compliant)
+ lualatex
  - Tex Gyre Pagella font
  - ~mdframed~ package
+ lilypond (and lilypond-book)
